import { Component, OnInit, TemplateRef } from '@angular/core';
import { Web3Service } from '../util/web3.service';

import { ActivatedRoute } from '@angular/router';
import * as ipfs from 'ipfs-api';
declare const Buffer;

declare let require: any;
const newspaper_artifacts = require('../../../build/contracts/NewsPaper.json');

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})

export class ViewComponent implements OnInit {
  accounts: string[];
  ipfsApi: any;

  model = {
    account: '',
    chosenPaper: '',
    chosenIndex: 0
  };

  paper = {
    name: '',
    category: '',
    state: '',
    country: '',
    length: 0,
    comments: undefined,
    owner: ''
  }

  tipAmount = 0;
  isOwned = false;
  comment = '';


  isUploading = false;

  NewsPaper: any;
  news = [];
  comments = [];

  commentPagination = {
    total: 0,
    current: 0,
    size: 5
  }

  status = "";

  moreCommentsAvailable = true;
  constructor(private web3Service: Web3Service, private route: ActivatedRoute) {
    this.ipfsApi = ipfs('ipfs.infura.io', '5001', { protocol: 'https' })
  }


  private sub: any;

  ngOnInit(): void {
    this.watchAccount();
    this.sub = this.route.params.subscribe(async params => {

      this.model.chosenPaper = params['address'];
      this.model.chosenIndex = params['index'];
      this.accounts = await this.web3Service.getAccounts();
      this.model.account = this.accounts[0];

    });
    this.web3Service.artifactsToContract(newspaper_artifacts)
      .then((NewsPaperAbstraction) => {
        this.NewsPaper = NewsPaperAbstraction;
        this.getNewsPaperDetails(this.model.chosenPaper)
      });
  }

  watchAccount() {
    this.web3Service.accountsObservable.subscribe((accounts) => {
      this.accounts = accounts;
      this.model.account = accounts[0];
    });
  }

  setStatus(status) {
    //alert(status)
  }


  async getPaperinformation(paperAddress) {
    const deployed = await this.NewsPaper.at(paperAddress);
    const info = await deployed.information.call({ from: this.model.account });
    const owner = await deployed.owner.call();

    return {
      name: info[0],
      country: info[1],
      state: info[2],
      category: info[3],
      length: info[4],
      owner: owner

    }
  }

  async getNews() {
    const deployed = await this.NewsPaper.at(this.model.chosenPaper);
    this.news = await deployed.news.call(this.model.chosenIndex);
    let description = await this.web3Service.getContentFromIPFS(this.news[3])
    this.news.push(description['_body']);
    this.commentPagination.total = await deployed.getCommentsLength.call(this.model.chosenIndex);
    this.getComments();
  }

  async getComments() {
    const deployed = await this.NewsPaper.at(this.model.chosenPaper);
    const nextPageSize = (this.commentPagination.current + 1) * (this.commentPagination.size);
    const toFetch = (nextPageSize > this.commentPagination.total) ? this.commentPagination.total : nextPageSize;
    if (this.commentPagination.current === toFetch) {
      this.moreCommentsAvailable = false;
      return alert('No more comments');
    }
    for (let i = this.commentPagination.current; i < toFetch; i++) {
      const comment = await deployed.comments.call(this.model.chosenIndex, i);
      comment[0] = await this.web3Service.getContentFromIPFS(comment[0]);
      comment[0] = comment[0]._body;
      this.comments.push(comment);
    }
    if (toFetch === this.commentPagination.total) {
      this.moreCommentsAvailable = false;
    }
    this.commentPagination.current = toFetch;

  }

  async getNewsPaperDetails(chosenPaper) {
    this.model.chosenPaper = chosenPaper;
    if (!this.NewsPaper) {
      this.setStatus('Contract is not loaded, unable to send transaction');
      return;
    }
    this.setStatus('Getting Chosen Paper Details... (please wait)');
    try {
      let paper = await this.getPaperinformation(this.model.chosenPaper);
      this.paper.name = paper.name;
      this.paper.state = paper.state;
      this.paper.country = paper.country;
      this.paper.category = paper.category;
      this.paper.length = paper.length;
      this.paper.owner = paper.owner;
      this.isOwned = this.isOwner();
      this.getNews();
    } catch (e) {
      console.log(e);
      this.setStatus('Error sending coin; see log.');
    }
  }


  async applaud() {
    try {

      const deployed = await this.NewsPaper.at(this.model.chosenPaper);
      alert("Submitting transaction");
      let result = await deployed.applaud(this.model.chosenIndex, { from: this.model.account, gas: 87398 });
      if (result.logs.length === 1) {
        alert("Applauded successfully");
        this.news[6]++;
      } else {
        alert("Failed to applaud")
      }
    } catch (err) {
      alert("Failed to applaud")
    }
  }

  async blame() {
    try {
      const deployed = await this.NewsPaper.at(this.model.chosenPaper);
      alert("Submitting transaction");
      let result = await deployed.blame(this.model.chosenIndex, { from: this.model.account, gas: 87398 });
      if (result.logs.length === 1) {
        alert("Blamed successfully");
        this.news[7]++;
      } else {
        alert("Failed to blame")
      }
    } catch (err) {
      alert("Failed to blame")
    }
  }

  async addComment() {
    const deployed = await this.NewsPaper.at(this.model.chosenPaper);
    try {
      this.isUploading = true;
      this.status = "Saving comment to IPFS for crunching data";
      let comment = await this.saveToIpfs(this.comment);
      this.isUploading = false;
      this.status = "IPFS Hash: " + comment[0].hash;
      alert("Submitting transaction");
      let result = await deployed.addComment(this.model.chosenIndex, comment[0].hash, { from: this.model.account, gas: 178398 });
      if (result.logs.length === 1) {
        alert("Comment posted successfully")
        this.commentPagination.total += 1;
        this.news[5]++;
        this.getComments();
      } else {
        alert("Posting comment failed");
      }
    } catch (err) {
      alert("Posting comment failed");
    }
    setTimeout(() => { this.status = "" }, 3000);
  }

  isOwner() {
    const currentAccount = parseInt(this.model.account);
    const owner = parseInt(this.paper.owner);
    if (currentAccount === owner) {
      return true;
    }
    return false;
  }

  convertDate(date) {
    return this.web3Service.convertBigNumberDateToLocale(date);
  }

  async tip() {
    const deployed = await this.NewsPaper.at(this.model.chosenPaper);
    let amount = this.web3Service.convertAmount(this.tipAmount, 'finney');
    alert("Submitting transaction");
    let result = await deployed.giveTips({ from: this.model.account, gas: 78398, value: amount });
    if (result.logs.length === 1) {
      alert("Tipped successfully")
    }
  }

  saveToIpfs(comment) {
    const buffer = new Buffer(comment);
    return this.ipfsApi.add(buffer, { progress: (prog) => console.log(`received: ${prog}`) });
  }
}
