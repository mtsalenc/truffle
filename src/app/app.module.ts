import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {LocationStrategy, HashLocationStrategy} from '@angular/common';

import { UserModule } from './user/user.module';
import { AppRoutingModule } from './app.routes';
import { PaperModule } from './paper/paper.module';
import { ViewModule } from './view/view.module';
import { Web3Service } from './util/web3.service';
import { CollapseModule } from 'ngx-bootstrap/collapse';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserAnimationsModule,
    CommonModule,
    BrowserModule,
    FormsModule,
    HttpModule,
    UserModule,
    PaperModule,
    ViewModule,
    AppRoutingModule,
    CollapseModule.forRoot()
  ],
  providers: [Web3Service,{
    provide: LocationStrategy,
    useClass: HashLocationStrategy
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
