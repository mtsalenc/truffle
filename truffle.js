let HDWalletProvider = require("truffle-hdwallet-provider");
const secrets = require('./secrets');

module.exports = {
  networks: {
    development: {
      host: "localhost",
      gasLimit: 1000000000,
      port: 8545,
      network_id: "*" // Match any network id
    },
    rinkenby: {
      provider: function () {
        return new HDWalletProvider(secrets.mnemonic,secrets.access_token)
      },
      network_id: 3,
      gas: 4900000
    }
  },
  solc: {
    optimizer: {
      enabled: true,
      runs: 200
    }
  }
};
