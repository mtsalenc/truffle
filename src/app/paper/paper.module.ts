import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UtilModule} from '../util/util.module';
import {RouterModule} from '@angular/router';
import { ModalModule } from 'ngx-bootstrap/modal';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { PaperComponent } from './paper.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PaperRoutingModule } from './paper.routes';
import { Web3Service } from '../util/web3.service';

@NgModule({
  imports: [
    BrowserAnimationsModule,
    CommonModule,
    FormsModule,
    RouterModule,
    UtilModule,
    ModalModule.forRoot(),
    ReactiveFormsModule,
    PaperRoutingModule
  ],
  providers: [Web3Service],
  declarations: [PaperComponent],
  exports: [PaperComponent]
})
export class PaperModule {
}
