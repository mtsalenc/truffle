import { Component, OnInit, TemplateRef } from '@angular/core';
import { Web3Service } from '../util/web3.service';

import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ActivatedRoute } from '@angular/router';
declare const Buffer
import * as ipfs from 'ipfs-api';

declare let require: any;
const newspaper_artifacts = require('../../../build/contracts/NewsPaper.json');
const distributor_artifacts = require('../../../build/contracts/Distributer.json');

import {
  ReactiveFormsModule,
  FormsModule,
  FormGroup,
  FormControl,
  Validators,
  FormBuilder
} from '@angular/forms';
@Component({
  selector: 'app-paper',
  templateUrl: './paper.component.html',
  styleUrls: ['./paper.component.css']
})

export class PaperComponent implements OnInit {

  currentNewspaper: any;
  
  isOwned = false;
  modalRef: BsModalRef;
  config = {
    animated: true
  };
  countries = {};
  countryNames = [];
  accounts: string[];
  categories = ['General', 'Education', 'Entertainment'];
  papers: Array<Object>;

  model = {
    amount: 5,
    receiver: '',
    balance: 0,
    account: '',
    balanceText: '',
    ownedPapers: [],
    chosenPaper: ''
  };

  moreNewsAvailable = true;
  starred = false;

  paper = {
    address: '',
    name: '',
    category: '',
    state: '',
    country: '',
    owner: '',
    paused: false
  }

  pagingConfig = {
    total: 0,
    current: 0,
    size: 2
  }

  tipInformation = {
    current: 0,
    total: 0
  }

  tipAmount = 0;
  myform: FormGroup;
  fileHash: string;

  ipfsApi: any;
  isUploading = false;


  NewsPaper: any;
  Distributor: any;
  news = [];
  constructor(private web3Service: Web3Service, private modalService: BsModalService, private route: ActivatedRoute) {
    this.myform = new FormGroup({
      title: new FormControl('', [Validators.required]),
      category: new FormControl('', [
        Validators.required
      ]),
    });
    this.ipfsApi = ipfs('ipfs.infura.io', '5001', { protocol: 'https' })
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, this.config);

  }

  private sub: any;

  ngOnInit(): void {
    this.watchAccount();
    this.sub = this.route.params.subscribe(async params => {
      this.model.chosenPaper = params['address'];
      this.accounts = await this.web3Service.getAccounts();
      this.model.account = this.accounts[0];
      // In a real app: dispatch action to load the details here.
    });
    this.web3Service.artifactsToContract(distributor_artifacts)
    .then((DistributorAbstraction) => {
      this.Distributor = DistributorAbstraction;
    });
    this.web3Service.artifactsToContract(newspaper_artifacts)
      .then(async (NewsPaperAbstraction) => {
        this.NewsPaper = NewsPaperAbstraction;
        let value = localStorage.getItem("favorites");
        if(value){
          value = JSON.parse(value);
          if(value[this.model.chosenPaper]){
            this.starred=true;
          }

        }
        this.currentNewspaper = await this.NewsPaper.at(this.model.chosenPaper);
        this.getNewsPaperDetails(this.model.chosenPaper)
      });
  }

  watchAccount() {
    this.web3Service.accountsObservable.subscribe((accounts) => {
      this.accounts = accounts;
      this.model.account = accounts[0];
    });
  }


  async getPaperinformation(paperAddress) {
    const info = await this.currentNewspaper.information.call({ from: this.model.account });
    const owner = await this.currentNewspaper.owner.call();
    const paused = await this.currentNewspaper.paused.call();

    return {
      name: info[0],
      country: info[1],
      state: info[2],
      category: info[3],
      length: info[4],
      owner: owner,
      paused: paused
    }
  }

  async getNews() {
    const nextPageSize = (this.pagingConfig.current + 1) * (this.pagingConfig.size);
    const toFetch = (nextPageSize > this.pagingConfig.total) ? this.pagingConfig.total : nextPageSize;
    if (this.pagingConfig.current === toFetch) {
      this.moreNewsAvailable = false;
      return alert('No more news');
    }
    for (let i = this.pagingConfig.current; i < toFetch; i++) {
      let info = await this.currentNewspaper.news.call(i);
      this.news.push(info);
    }
    if (toFetch === this.pagingConfig.total) {
      this.moreNewsAvailable = false;
    }
    this.pagingConfig.current = toFetch;
  }

  async getNewsPaperDetails(chosenPaper) {
    this.model.chosenPaper = chosenPaper;
    if (!this.NewsPaper) {
      return;
    }
    try {
      let paper = await this.getPaperinformation(this.model.chosenPaper);
      this.paper.paused = paper.paused;
      this.paper.name = paper.name;
      this.paper.state = paper.state;
      this.paper.country = paper.country;
      this.paper.category = paper.category;
      this.pagingConfig.total = paper.length;
      this.paper.owner = paper.owner;
      this.paper.address = chosenPaper;

      this.isOwned = this.isOwner();
      if (this.isOwned) {
        this.setTipInformation()
      }
      if (paper.length > 0) {
        this.getNews();
      }
    } catch (e) {
      console.log(e);
    }
  }


  setAmount(e) {
    console.log('Setting amount: ' + e.target.value);
    this.model.amount = e.target.value;
  }

  setReceiver(e) {
    console.log('Setting receiver: ' + e.target.value);
    this.model.receiver = e.target.value;
  }




  async tip() {
    try{
      let amount = this.web3Service.convertAmount(this.tipAmount, 'finney');
      alert("Submitting transaction");
      let result = await this.currentNewspaper.giveTips({ from: this.model.account, gas: 278398, value: amount });
      if (result.logs.length === 1) {
        alert("Tipped successfully")
      } else {
        alert("Tipping failed")
      }
    } catch(err){
      alert("Tipping failed")
    }
  }

  isOwner() {
    const currentAccount = parseInt(this.model.account);
    const owner = parseInt(this.paper.owner);
    if (currentAccount === owner) {
      return true;
    }
    return false;
  }

  captureFile(event) {
    this.isUploading = true;
    this.fileHash = undefined;
    const file = event.target.files[0]
    let reader = new FileReader();
    reader.onloadend = () => this.saveToIpfs(reader)
    reader.readAsArrayBuffer(file)
  }

  saveToIpfs(reader) {
    const buffer = Buffer.from(reader.result)
    this.ipfsApi.add(buffer, { progress: (prog) => console.log(`received: ${prog}`) })
      .then((response) => {
        this.isUploading = false;
        this.fileHash = response[0].hash;
      }).catch((err) => {
        console.error(err)
      })
  }

  async publishNews(values) {
    try {
      if(values.title.length>60){
        return alert('News title too large => more gas is required')
      }
      else if(values.category.length>50){
        return alert('News category too large => more gas is required')
      }
      alert("Submitting transaction");
      let result = await this.currentNewspaper.publishNews(values.title, values.category, this.fileHash, { from: this.model.account, gas: 478398 });
      if (result.logs.length === 1) {
        this.ngOnInit();
        alert("Published successfully")
      } else {
        alert("Publishing failed")
      }
    } catch (err) {
      console.log(err)
      alert("Publishing failed")
    }
  }

  convertDate(date) {
    return this.web3Service.convertBigNumberDateToLocale(date);
  }

  async setTipInformation() {
    let result = await this.currentNewspaper.getTipInformation.call({ from: this.model.account });
    this.tipInformation.current = this.web3Service.convertAmountFromWei(result[0].toString(), 'ether');
    this.tipInformation.total = this.web3Service.convertAmountFromWei(result[1].toString(), 'ether');
  }

  async withdraw() {
    try {
      alert("Submitting transaction");
      let result = await this.currentNewspaper.withdrawTips({ from: this.model.account, gas: 50000 });
      if (result.logs.length === 1) {
        alert("Withdrew successfully")
      } else {
        alert("Failed to withdraw");
      }
    }
    catch (err) {
      alert("Failed to withdraw");
    }
  }

  async pauseOrResume() {
    try {
      let result;
      if (!this.paper.paused) {
        alert("Submitting transaction");
        result = await this.currentNewspaper.pause({ from: this.model.account, gas: 50000 });
        if (result.logs.length === 1) {
          alert("Paused successfully");
          this.paper.paused = true;
        }
      } else {
        alert("Submitting transaction");
        result = await this.currentNewspaper.unpause({ from: this.model.account, gas: 50000 });
        if (result.logs.length === 1) {
          alert("Resumed successfully");
          this.paper.paused = false;
        }
      }
    } catch (err) {
      alert("Operation failed")
    }

  }

  async addToFavorite(){
    let value = localStorage.getItem("favorites");
    let favs = {};
    favs = JSON.parse(value);
    if(favs===undefined||favs===""||favs===null){
      favs = {}
    }
    if(this.starred){
      delete favs[this.model.chosenPaper];
    } else {
      favs[this.model.chosenPaper] = this.paper;
    }

    value = JSON.stringify(favs)
    localStorage.setItem("favorites", value);
    alert("Saving favorites to IPFS for crunching data");
    let data = await this.saveToIpfsFavs(value);
    alert("IPFS Hash: " + data[0].hash);
    try{
      const deployed = await this.Distributor.deployed();
      alert("Submitting transaction");
      let result = await deployed.alterFavourite(data[0].hash, { from: this.model.account, gas: 90000 });
      if (result.logs.length === 1) {
        alert("Added to favorite successfully");
      }
    } catch(err){
      console.error(err)
    }
  }

  saveToIpfsFavs(comment) {
    const buffer = new Buffer(comment);
    return this.ipfsApi.add(buffer, { progress: (prog) => console.log(`received: ${prog}`) });
  }

}
