pragma solidity 0.4.24;
//import "./Owned.sol";
//import "./SafeMath.sol";
import "openzeppelin-solidity/contracts/math/SafeMath.sol";
import "openzeppelin-solidity/contracts/lifecycle/Destructible.sol";
import "openzeppelin-solidity/contracts/lifecycle/Pausable.sol";

/**
 * @title Newspaper
 * @dev The Newspaper contract is a basic contract which lets one to create a newspaper in ethereum
 */

contract Newspaper is Destructible, Pausable {
    
    using SafeMath for uint;
    
    uint private totalTips;
    uint private currentTips;

	struct InformationStruct {
		string name;
		string country;
		string state;
		string category;
		uint totalNews;
		uint totalBlames;
		uint totalApplauds;
	}
    
    struct NewsStruct{
        uint index;
        string title;
        string category;
        string ipfsLocation;
        uint timestamp;
        uint totalComments;
        uint applauds;
        uint blames;
    }
    
    struct CommentStruct {
        string comment;
        uint timestamp;
        address user;
    }
    
    string[] public categories;
    mapping (uint => CommentStruct[]) public comments;
    NewsStruct[] public news;

	InformationStruct public information;
    
    mapping (bytes32 => bool) categoryExists;
    mapping (bytes32 => bool) newsExists;
	mapping (bytes32 => uint[]) public categoryNews;
	mapping (bytes32 => uint) public titleNews;
	mapping (bytes32 => bool) public didVote;
	
	event LogNewsPublished(
		address indexed author, 
		string titleNews, 
		string category, 
		uint timestamp
	);

	event LogTipReceived(
		address indexed sender, 
		uint amount, 
		uint timestamp
	);

	event LogNewsBlamed(
		uint indexed newsID, 
		address indexed blamer, 
		uint timestamp
	);

	event LogNewsApplauded(
		uint indexed newsID, 
		address indexed blamer, 
		uint timestamp
	);

    event LogTipsWithdrawn(address indexed author, uint amount);
    event LogCommented(address indexed user, uint newsIndex, uint commentIndex);

	
	modifier requireNoOverflow {
	    require(totalTips>0);
	    uint newBalance = owner.balance.add(totalTips);
	    require(newBalance>owner.balance);
	    _;
	}
	
	modifier requireNotVotedPreviously(uint id){
	    bytes32 combo = keccak256(abi.encodePacked(msg.sender,id));
	    require(didVote[combo] == false);
	    didVote[combo] = true;
	    _;
	}

 	/** @dev This is constructor function of newspaper
      * @param name Newspaper name
      * @param country Country of publication
	  * @param state State of publication
      * @param category Type of newspaper
    */
	constructor(string name, string country, string state, string category) public{
	    information = InformationStruct(name, country, state, category, 0, 0, 0 );
	}
	
	/** @dev This is function is called by the owner to publish new news
      * @param title Newspaper name
      * @param newsCategory Country of publication
	  * @param ipfsLocation State of publication
      * @return success Indicates if action was successful
    */
	function publishNews(string title, string newsCategory, string ipfsLocation)
	whenNotPaused
	onlyOwner
	public
	returns(bool success){
	   bytes32 titleHash = keccak256(abi.encodePacked(title));
	   require(newsExists[titleHash] == false);
	   newsExists[titleHash] = true;
	   bytes32 categoryHash = keccak256(abi.encodePacked(newsCategory));
	   if(categoryExists[categoryHash] == false){
	       categoryExists[categoryHash] = true;
	       categories.push(newsCategory);
	   }
	   uint length = news.length;
	   NewsStruct memory currentNews = NewsStruct(length, title, newsCategory, ipfsLocation, now, 0, 0, 0);
	   news.push(currentNews);
	   categoryNews[categoryHash].push(length);
	   titleNews[titleHash] = length;
	   emit LogNewsPublished(owner, title, newsCategory, now);
	   information.totalNews = information.totalNews.add(1);
	   return true;
	}
	
	/** @dev This is function is called by others to give tips to the author
      * @return success Indicates if action was successful
    */
	function giveTips() 
	whenNotPaused
	payable
	public
	returns(bool success){
	    require(msg.value>0);
	    totalTips = totalTips.add(msg.value);
	    currentTips = currentTips.add(msg.value);
	    emit LogTipReceived(msg.sender, msg.value, now);
	    return true;
	}
	

	/** @dev This is function is called by others to vote down a news published by the author
      * @param id index of the specified news in the array of the news items
      * @return success Indicates if action was successful
    */
	function blame(uint id) 
	whenNotPaused
	requireNotVotedPreviously(id)
	public
	returns(bool success){
	    NewsStruct storage chosenNews = news[id];
	    information.totalBlames = information.totalBlames.add(1);
	    chosenNews.blames = chosenNews.blames.add(1);
	    emit LogNewsBlamed(id, msg.sender, now);
	    return true;
	}
	
	/** @dev This is function is called by others to vote up a news published by the author
      * @param id index of the specified news in the array of the news items
      * @return success Indicates if action was successful
    */
	function applaud(uint id) 
	whenNotPaused
	requireNotVotedPreviously(id)
	public
	returns(bool success){
	    NewsStruct storage chosenNews = news[id];
	    information.totalApplauds = information.totalApplauds.add(1);
	    chosenNews.applauds = chosenNews.applauds.add(1);
	    emit LogNewsApplauded(id, msg.sender, now);
	    return true;
	}
	
	/** @dev This is function is called by others to add comments on a specific news
      * @param id index of the specified news in the array of the news items
	  * @param comment Comment by user
      * @return success Indicates if action was successful
    */
	function addComment(uint id, string comment) 
	whenNotPaused
	public
	returns(bool success){
	    CommentStruct memory newComment = CommentStruct(comment,now,msg.sender);
	    NewsStruct storage chosenNews = news[id];
	    uint commentIndex = comments[id].length;
	    chosenNews.totalComments = chosenNews.totalComments.add(1);
	    comments[id].push(newComment);
	    emit LogCommented(msg.sender, id, commentIndex);
	    return true;
	}
	
	/** @dev This is function is called by owner to collect tips
      * @return success Indicates if action was successful
    */
	function withdrawTips()
	onlyOwner
	whenNotPaused
	requireNoOverflow
	public
	returns(bool success){
		require(currentTips>0);
        uint money = currentTips;
        currentTips = 0;
        owner.transfer(money);
        emit LogTipsWithdrawn(owner, money);
        return true;
	}
	
	/** @dev This is function is called by owner to get tip information
      * @return current Active tips in the contract
	  * @return tatal Total tips collected by the newspaper from inception
    */
	function getTipInformation()
	onlyOwner
	view
	public
	returns(uint current, uint total){
	    return (currentTips, totalTips);
	}

	/** @dev This function returns length of comments for a particular news
	  * @param id index of the specified news in the array of the news items
	  * @return tatal Total tips collected by the newspaper from inception
    */
	function getCommentsLength(uint id)
	view
	public
	returns(uint total){
	    return comments[id].length;
	}
}
