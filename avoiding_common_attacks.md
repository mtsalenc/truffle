# Avoiding common attacks
Every contract is susceptible to attack, however, by following certain standards in implementing the function this can be drastically reduced.

Below are the steps taken to reduce the presence of bugs.

## Prevention of Logic Bugs using tests
Truffle tests are used to make interactions with the contracts in both correct and wrong way. Then the results are compared to see if it matches the expected behaviour.

## Overflow/Underflow Prevention
Battle-tested safemath library by Zeppelin is used to perform common arithmetic operations.

## Functions with proper modifiers
Functions are checked to make sure that only functions that are meant to be exposed are available to the public. Modifiers such as internal and private are used to limit access to internal functions

## Using pre-defined contract by OpenZeppelin
OpenZeppelin includes contracts that are battle-tested, so rather than implementing them on my own, the zeppelin contracts are made use for ownership related functionalities and to implement safe math operations

## Re-entrancy prevention using optimistic accounting
Optimistic accounting is used to counter re-entrancy attack

## Access control using Modifiers
Modifiers are used to limit calling of certain functions such as self-destruct of contract in case of presence of bugs and publishing of news

## Implementation of killswitch and pause button to pull the plug if something goes wrong
Contracts are implemented with a kill switch that can be called by contract owners in case something goes wrong. Moreover, the owner is able to pause the operation in case of small errors rather than killing the entire contract.

## Certain unsecure solidity features are avoided
Things like tx.origin and .send() are avoided and replaced with msg.sender and .transfer()
