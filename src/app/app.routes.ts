import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// Layouts


export const routes: Routes = [
  {
    path: '',
    redirectTo: 'user',
    pathMatch: 'full',
  },
  {
    path: 'user',
    redirectTo: 'user',
    pathMatch: 'full',
  },
  {
    path: 'paper/:address',
    redirectTo:'paper'
  },
  {
    path: 'view/:address/:index',
    redirectTo:'view'
  },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})

export class AppRoutingModule {}
