# Design Pattern Decisions

## A brief introduction of the application
This DApp is a tool whereby others can create newspaper, manage created newspapers, view newspapers, interact with news and facilitate as a directory for all created newspapers. 

## Pattern 1: Factory Pattern
Factory pattern is chosen as the ideal pattern as all newspapers share the same smart contract structure.  Following a standard helps to abstract away many of the implementation details. Moreover, having a skeleton contract that can be easily deployed can help achieve the vision of having a decentralized and localized newspaper realize faster. 

## Pattern 2: Mortal
This pattern allows to the contract owner to destroy the contract in case something goes wrong. This is done via inheriting Destructible contract by open-zeppeling in the main contracts.

## Pattern 3: Circuit breaker
This pattern allows to the contract owner to pause the operation if something where to go wrong. This is done via inheriting Pausable contract by open-zeppeling in the main contracts.

## Pattern 4: Restricting Access
This pattern allows to limiting the access of certain functions to intended users. This is done via use of modifiers.

## Pattern 5: Fail early and fail loud
This pattern prevents unwanted executions if the conditions are not met. This is done using require functionality offered by solidity.

Additionaly, for different aspects of the contract the below designs decisions are made:

### For Actions
1. Check for validity of the call, and fail hard if a test fails.
2. Optimistic accounting, i.e. allocating appropriate numbers to state varibales before performing any action, when it comes to dealing with numbers
3. Interact once with a single external contract and fail hard if it did not take place correctly.
4. All state changes are logged and returns a value

### For Data

1. Storage data is made public so that legimate users easily interact with the contract. There is no point in making data private as it can always be found out.
2. Type conversions are done appropriately
3. Checks are made at dApp side to limit the number of characters in string. Moreover, large data is always stored in IPFS, the hash is then stored in blockchain for retrieval purposes

### For Value
1. Re-entrancy is checked using Checks-Effects-Interactions pattern
2. Do not leave trapped value in your contracts
3. Contract hardened against reception of ether wherever not neccessary 

### Gas economy
1. Looping prevented or checked
2. Used reverts to punish wrong actions

### Safety
1. selfdestruct function is implemented as a safety mechanism to destroy the contract in case of some bugs.
2. pause/resume function is added as well to stop operation temporarily

### Other design choices
Open-Zeppelin contracts used to carry out contract management and implement Safemath functions. EthPM packages of zeppelin is old, so the newer version is used by open-zepellin npm module

## Off-chain data storage: IPFS via Infura
IPFS is used to store offchain data and large text contents. The obtained hash is then stored in the blockchain for retrieval purposes. 