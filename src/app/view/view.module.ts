import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UtilModule} from '../util/util.module';
import {RouterModule} from '@angular/router';
import { ModalModule } from 'ngx-bootstrap/modal';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { ViewComponent } from './view.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ViewRoutingModule } from './view.routes';
import { Web3Service } from '../util/web3.service';

@NgModule({
  imports: [
    BrowserAnimationsModule,
    CommonModule,
    FormsModule,
    RouterModule,
    UtilModule,
    ModalModule.forRoot(),
    ReactiveFormsModule,
    ViewRoutingModule
  ],
  providers: [Web3Service],
  declarations: [ViewComponent],
  exports: [ViewComponent]
})
export class ViewModule {
}
