# About the application : Project DNews

Newspaper is something that many people rely to get updates on events happening around them. However, spread of news through Internet can be limited by a corrupt government through limiting access to internet, as happened in Egypt during Arab Spring. Since most websites are hosted on Cloud or shared hosting servers which are usually outside the country or belong to a certain IP group that can be easily blocked, a corrupt government can stop spread of information via internet easily. Moroever, localisation of news generation can help to convey better explanation to people than some international brand reporting the same news. Localisation of news generation also helps freelance journalists as they can generate value from their skill with the support of the community. By leveraging technologies like Etherum and IPFS, all above mentioned issue can be solved as the news can be exchanged in a P2P manner. Additionally, having a directory where all newspapers are listed helps users to easily find their desired newspaper. 

Here are the features provided by this DApp
## For news publishers
1. Create any number of newspapers for each category or locality
2. Publish news to created newspapers
3. List their newspaper in the directory
4. Recieve tips from the news readers

## For news readers
1. This DApp provides a interface to choose their favorite newspaper easily
2. View the published news
3. Allows to rate news
4. Hold discussions on the published news 
5. Add news paper to favorites 
6. Quickly view favorite newspapers

Different view will be rendered by Angular for different users when on the newspaper details and news page. So, please make sure to explore the application as the newspaper owner and as a different user.

## Live Project location in IPFS
1. https://ipfs.infura.io/ipfs/QmfAUv3JQap18z845HQVEAS6Yq1dBKmiXcqai7rZx55Y21 or
2. https://gateway.ipfs.io/ipfs/QmfAUv3JQap18z845HQVEAS6Yq1dBKmiXcqai7rZx55Y21


## Limitations
This project does not go into exploring the credibility of the news publishers or background. This feature will be added later if the project moves forward.
Moreover usage of local IPFS when available is another area to be looked at if the project goes forward in order to make it truly decentralized.

# Setting up
This project is built using truffle and Angular JS leveraging infura APIs for Ethereum and IPFS.

## Environment
Run `npm install` to install appropriate node modules<br>
Run `truffle install` to download appropriate ethereum contracts

## Compiling the contract
Run `truffle compile` to compile the contracts 

## Migrating the contract
Run `truffle migrate` to migrate the contracts on the desired blockchain. Make sure to make appropriate changes to port number in `truffle.js` to match your testrpc/ganache port number.

If you want to migrate to rinkenby network, make sure to populate `secrets/index.js` with your infura access token and your account mnemonic.

## Running the tests
Run `truffle test` to migrate the contracts on the desired blockchain

## Running the DApp
RUN `ng serve` to start the application<br>
Visit `http://localhost:4200' to interact with the application