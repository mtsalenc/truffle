import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UtilModule} from '../util/util.module';
import {RouterModule} from '@angular/router';
import { ModalModule } from 'ngx-bootstrap/modal';
import { AlertModule } from 'ngx-bootstrap/alert';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { UserComponent } from './user.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserRoutingModule } from './user.routes';
import { Web3Service } from '../util/web3.service';

@NgModule({
  imports: [
    AlertModule.forRoot(),
    BrowserAnimationsModule,
    CommonModule,
    FormsModule,
    RouterModule,
    UtilModule,
    ModalModule.forRoot(),
    ReactiveFormsModule,
    UserRoutingModule
  ],
  providers:[Web3Service],
  declarations: [UserComponent],
  exports: [UserComponent]
})
export class UserModule {
}
