pragma solidity ^0.4.2;

import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/Newspaper.sol";


contract TestNewspaper {

  Newspaper newspaper;
  //Paper Related
  string paperName = "U.A.E Times";
  string countryName = "U.A.E";
  string stateName = "Dubai";
  string categoryName = "General";

  //News Related
  string  title = "Decentralization is leading";
  string  title2 = "Ethereum is the new standard";
  string  category = "General";
  string  ipfsLocation = "QmR7GSQM93Cx5eAg6a6yRzNde1FQv7uL6X1o4k7zrJa3LX";

  uint public initialBalance = 10 ether; // or any other value

  /** @dev This hook creates an instance of newspaper before every other tests
  */
  function beforeAll() public {
    newspaper = new Newspaper(paperName, countryName, stateName, categoryName);
  }

  /** @dev This test checks if the owner matches
  */
  function testOwnerMatches() public {
    address owner = newspaper.owner();
    Assert.equal(owner, this, "Owner Address should be same");
  }

  /** @dev This test checks if information of created newspaper matches
  */
  function testInfoCorrectlyDisplayed() public {
    var (name, country, state, category, totalNews, applauds, blames) = newspaper.information();
    Assert.equal(0, uint(totalNews), "Total news should be 0");
    Assert.equal(paperName, string(name), "Name should be same");
    Assert.equal(countryName, string(country), "Country should be same");
    Assert.equal(stateName, string(state), "State should be same");
    Assert.equal(categoryName, string(category), "Category should be same");
    Assert.equal(0, uint(applauds), "Applauds should be 0");
    Assert.equal(0, uint(blames), "Blames should be 0");
  }

  /** @dev This test checks if the publishing of news to newspaper is successful
  */
  function testPublishNews() public {
   bool result = newspaper.publishNews(title, category, ipfsLocation);
   Assert.isTrue(result, "Publishing should be successful");
   
    result = newspaper.call(bytes4(keccak256("publishNews(string, string, string)")),title, category, ipfsLocation);
    Assert.isFalse(result, "Should not be able to spam same news");
  }

  
  /** @dev This test checks the content of published news and verifies if it matches
  */
   function testPublishedNews() public {
    var (index, nTitle, nCategory, nIpfsLocation, timestamp, totalComments, applauds, blames) = newspaper.news(0);
    Assert.equal(0, uint(index), "Index should be same");
    Assert.equal(title, string(nTitle), "Name should be same");
    Assert.equal(category, string(nCategory), "Category should be same");
    Assert.equal(ipfsLocation, string(nIpfsLocation), "IPFS Location should be same");
    Assert.notEqual(0,uint(timestamp),"Timestamp should not be 0");
    Assert.equal(0, uint(totalComments), "Comments should be 0");
    Assert.equal(0, uint(applauds), "Applauds should be 0");
    Assert.equal(0, uint(blames), "Blames should be 0");
  }

  /** @dev This test checks the blame functionality
  */
  function testBlame() public payable {
    bool result = newspaper.blame(0);
    Assert.isTrue(result, "Should be able to blame");

    result = newspaper.call(bytes4(keccak256("blame(uint)")),0);
    Assert.isFalse(result, "Should not be able to blame");
  }

  /** @dev This test checks the publishing of another news
  */
  function testPublishAnotherNews() public {
   bool result = newspaper.publishNews(title2, category, ipfsLocation);
   Assert.isTrue(result, "Publishing should be successful");
  }

  /** @dev This test checks the applaud functionality
  */
  function testApplaud() public payable {
    bool result = newspaper.applaud(1);
    Assert.isTrue(result, "Should be able to applaud");

    result = newspaper.call(bytes4(keccak256("applaud(uint)")),1);
    Assert.isFalse(result, "Should not be able to applaud");
  }

  /** @dev This test checks commenting functionality
  */
  function testComment() public payable {
    bool result = newspaper.addComment(1,"Good stuff");
    Assert.isTrue(result, "Should be able to comment");
  }

  /** @dev This test checks giving of tips to user
  */
  function testGiveTips() public payable {
    bool result = newspaper.giveTips.value(initialBalance)();
    Assert.isTrue(result, "Should be able to send ether");

    var (currentTip, totalTip) = newspaper.getTipInformation();
    Assert.equal(currentTip, initialBalance, "Tip should be equal to initial balance");
    Assert.equal(currentTip, totalTip, "Current tip should equal to total tips");

  }

}



