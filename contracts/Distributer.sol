pragma solidity 0.4.24;
import './Newspaper.sol';

/**
 * @title Distributer
 * @dev The Distributer contract is a factory contract which lets others to create their own newspapers in ethereum
 *      and facilitate as a directory where others can find newspapers.
 */
contract Distributer is Destructible, Pausable {
    
    mapping (bytes32 => address[]) public countryIndex;
    mapping (bytes32 => address[]) public locationIndex;
    mapping (bytes32 => address[]) public locationCategoryIndex;
    mapping (bytes32 => address[]) public categoryIndex;
    mapping (address => address[]) public ownedPapers;
    mapping (address => string) public favouritesIPFSHash;
    mapping (bytes32 => bool) paperExists;
    
    address[] public newsPapers;
    
    modifier requirePaperNotExist(string country, string state, string category, string name){
        bytes32 hash = keccak256(abi.encodePacked(country, state, category, name));
        require(paperExists[hash]==false);
        paperExists[hash]=true;
        _;
    }
    event LogNewNewsPaperCreated(address indexed contractAddress, address indexed creator, string name);
    event LogFavoriteAltered(address indexed user, string oldIpfsHash, string newIpfsHash);
    
	//"U.A.E","Sharjah","General","Gulf News 2"
    
 	/** @dev This is function is called by others to create a newspaper and list it in this directory
      * @param name Newspaper name
      * @param country Country of publication
	  * @param state State of publication
      * @param category Type of newspaper
      * @return success Indicates if action was successful
    */
    function createANewsPaper(string name, string country, string state, string category)
    whenNotPaused
    public
    returns(bool success){
        Newspaper news = new Newspaper(name, country, state, category);
        news.transferOwnership(msg.sender);
        bytes32 countryHash = keccak256(abi.encodePacked(country));
        bytes32 locationHash = keccak256(abi.encodePacked(country, state));
        bytes32 locationCategoryHash = keccak256(abi.encodePacked(country, state, category));
        bytes32 categoryHash = keccak256(abi.encodePacked(category));

        countryIndex[countryHash].push(news);
        locationIndex[locationHash].push(news);
        locationCategoryIndex[locationCategoryHash].push(news);
        categoryIndex[categoryHash].push(news);
        ownedPapers[msg.sender].push(news);
        
        newsPapers.push(news);
        
        emit LogNewNewsPaperCreated(news, msg.sender, name);
        
        return true;
    }
    
    /** @dev This is function is called by others to number of papers in directory on the basis of country
      * @param hash Hash of the country value
      * @return count Number of papers fitting this criteria
    */
    function getCountryWisePaperCount(bytes32 hash)
    public
    view
    returns(uint count){
        return countryIndex[hash].length;
    }

    /** @dev This is function is called by others to number of papers in directory on the basis of location
      * @param hash Hash of the country and state values
      * @return count Number of papers fitting this criteria
    */
    function getLocationPaperCount(bytes32 hash)
    public
    view
    returns(uint count){
        return locationIndex[hash].length;
    }
    
    /** @dev This is function is called by others to number of papers in directory on the basis of location and category
      * @param hash Hash of the country, state and category values
      * @return count Number of papers fitting this criteria
    */
    function getLocationCategoryPaperCount(bytes32 hash)
    public
    view
    returns(uint count){
        return locationCategoryIndex[hash].length;
    }
    
    /** @dev This is function is called by others to number of papers in directory on the basis of category
      * @param hash Hash of the category value
      * @return count Number of papers fitting this criteria
    */
    function getCategoryPaperCount(bytes32 hash)
    public
    view
    returns(uint count){
        return categoryIndex[hash].length;
    }
    
    /** @dev This is function returns number of papers owned by the caller
      * @return count Number of papers fitting this criteria
    */
    function getOwnedCount()
    public
    view
    returns(uint count){
        return ownedPapers[msg.sender].length;
    }

    /** @dev This is function returns number of papers listed/created in this directory
      * @return count Number of papers fitting this criteria
    */
    function getPapersCount()
    public
    view
    returns(uint count){
        return newsPapers.length;
    }

    /** @dev This is function is by the user to set his favorites
      * @param newIPFSHash IPFS location  where is favorite information is stored
      * @return success Indicates if action was successful
    */
    function alterFavourite(string newIPFSHash)
    public
    returns(bool success){
        emit LogFavoriteAltered(msg.sender, favouritesIPFSHash[msg.sender], newIPFSHash);
        favouritesIPFSHash[msg.sender] = newIPFSHash;
        return true;
    }
}
