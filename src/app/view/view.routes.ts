import { Routes,
     RouterModule } from '@angular/router';
import { ViewComponent } from './view.component';
import {NgModule} from '@angular/core';



const routes: Routes = [
  {
    path: 'view/:address/:index',
    component: ViewComponent,
    data: {
      title: 'view'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ViewRoutingModule {}
