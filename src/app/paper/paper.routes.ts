import { Routes,
     RouterModule } from '@angular/router';
import { PaperComponent } from './paper.component';
import {NgModule} from '@angular/core';



const routes: Routes = [
  {
    path: 'paper/:address',
    component: PaperComponent,
    data: {
      title: 'Paper'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PaperRoutingModule {}
