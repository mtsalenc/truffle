const Newspaper = artifacts.require("./Newspaper.sol");

const newPaperInfo = {
  name: 'Gulf Times',
  country: 'U.A.E',
  state: 'Sharjah',
  category: 'General'
}

function getBalancePromise(account) {
  return new Promise(function (resolve, reject) {
    Newspaper.web3.eth.getBalance(account, function (err, balance) {
      if (err) {
        reject(err);
      }
      resolve(balance.toNumber());
    });
  })

}
const MILLION_WEI = 1000000;
const tipToSend = 10 * MILLION_WEI;
//This test checks the whole tipping lifecycle
contract('Newspaper', function (accounts) {
  let newspaper
  before('setup contract for each test', async function () {
    try {
      newspaper = await Newspaper.new(newPaperInfo.name, newPaperInfo.country, newPaperInfo.state, newPaperInfo.category);
    } catch (err) {
      throw err;
    }
  });
  it("should have same attributes used while instantiating", function () {
    return newspaper.owner.call().
    then(function (owner) {
      assert.equal(owner, accounts[0], "Owner should match");
      return newspaper.information.call();
    }).
    then(function (info) {
      assert.equal(parseInt(info[5]), 0, "Blames should match");
      assert.equal(info[0], newPaperInfo.name, "Name should match");
      assert.equal(info[1], newPaperInfo.country, "Country should match");
      assert.equal(info[2], newPaperInfo.state, "State should match");
      assert.equal(info[3], newPaperInfo.category, "Category should match");
      assert.equal(parseInt(info[4]), 0, "Applauds should match");
      assert.equal(parseInt(info[5]), 0, "Blames should match");
    });
  });
  it("should be able to send tips", function () {
    return newspaper.giveTips({
      from: accounts[1],
      value: tipToSend
    }).
    then(function (result) {
      assert.equal(result.logs[0].event, "LogTipReceived", "Tipping should succeed");
      return newspaper.getTipInformation();
    }).then(function (value) {
      assert.equal(value[0].toNumber(), value[1].toNumber(), "Current tip should be equal to total tips");
      assert.equal(value[0].toNumber(), tipToSend, "Current tip should be equal to tip given");
    })
  });
  it("should be able to withdraw tips by owner", function () {
    let initalBalance;
    getBalancePromise(accounts[0]).then(function (balance) {
      initalBalance = balance;
      return newspaper.withdrawTips({
        from: accounts[0],
        gas: 70000,
        gasPrice: 5
      })
    }).then(function (result) {
      assert.equal(result.logs[0].event, "LogTipsWithdrawn", "Withdrawal should succeed");
      return newspaper.getTipInformation();
    }).then(function (value) {
      assert.equal(value[0].toNumber(), 0, "Current tip should be zero");
      assert.equal(value[1].toNumber(), tipToSend, "Total tip should be equal to tip given");
      return getBalancePromise(accounts[0]);
    }).then(function (newBalance) {
      let balanceGreater = newBalance > initalBalance;
      assert.equal(balanceGreater, true, "Balance got increased");
    })
  })

});
