pragma solidity ^0.4.2;

import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/Distributer.sol";


contract TestDistributer {

  Distributer distributer;

  /** @dev This hook creates an instance of distributor
  */
  function beforeAll() public {
    distributer = Distributer(DeployedAddresses.Distributer());
  }

  /** @dev This test checks of owner changing functionality
  */
  function testChangeOwner() public {
    bool result = distributer.call(bytes4(keccak256("changeOwner(address)")), this);
    Assert.isFalse(result, "Should not be able to change owner");
  }

  /** @dev This test checks of contract killing functionality
  */
  function testKill() public {
    bool result = distributer.call(bytes4(keccak256("kill()")));
    Assert.isFalse(result, "Should not be able to kill contract");
  }

  /** @dev This test checks if a user is able to create a newspaper and if it gets added to the directory
  */
  function testCreationOfNewspaper() public {
    distributer.createANewsPaper("Gulf News 2", "U.A.E", "Sharjah", "General");
    uint count = distributer.getPapersCount();
    uint expected = 1;
    Assert.equal(count, expected, "Count of newspaper is as expected");
  }

  /** @dev This test checks the total number of paper created by user is same
  */
  function testGetOwnedPaperCount() public {
    uint count = distributer.getOwnedCount();
    uint expected = 1;
    Assert.equal(count, expected, "Count of owned newspaper is as expected");
  }

  /** @dev This test checks the total number of paper by category is as expected
  */
  function testGetCategoryPaperCount() public {
    bytes32 hash = keccak256(abi.encodePacked("General"));
    uint count = distributer.getCategoryPaperCount(hash);
    uint expected = 1;
    Assert.equal(count, expected, "Count of newspaper with right category is as expected");
  
    hash = keccak256(abi.encodePacked("general"));
    count = distributer.getCategoryPaperCount(hash);
    expected = 0;
    Assert.equal(count, expected, "Count of newspaper with wrong category is as expected");
  }

  /** @dev This test checks the total number of paper by category and location is as expected
  */
  function testGetLocationCategoryPaperCount() public {
    bytes32 hash = keccak256(abi.encodePacked("U.A.E", "Sharjah", "General"));
    uint count = distributer.getLocationCategoryPaperCount(hash);
    uint expected = 1;
    Assert.equal(count, expected, "Count of newspaper with right category and location is as expected");

    hash = keccak256(abi.encodePacked("U.A.E", "Sharjah", "general"));
    count = distributer.getLocationCategoryPaperCount(hash);
    expected = 0;
    Assert.equal(count, expected, "Count of newspaper with wrong category and location is as expected");
  }

  /** @dev This test checks the total number of paper by location is as expected
  */
  function testGetLocationPaperCount() public {
    bytes32 hash = keccak256(abi.encodePacked("U.A.E", "Sharjah"));
    uint count = distributer.getLocationPaperCount(hash);
    uint expected = 1;
    Assert.equal(count, expected, "Count of newspaper with right category and location is as expected");

    hash = keccak256(abi.encodePacked("U.A.E", ""));
    count = distributer.getLocationPaperCount(hash);
    expected = 0;
    Assert.equal(count, expected, "Count of newspaper with wrong category and location is as expected");
  }

  /** @dev This test checks the total number of paper by country is as expected
  */
  function testGetCountryPaperCount() public {
    bytes32 hash = keccak256(abi.encodePacked("U.A.E"));
    uint count = distributer.getCountryWisePaperCount(hash);
    uint expected = 1;
    Assert.equal(count, expected, "Count of newspaper with right country is as expected");

    hash = keccak256(abi.encodePacked("K.S.A"));
    count = distributer.getCountryWisePaperCount(hash);
    expected = 0;
    Assert.equal(count, expected, "Count of newspaper with wrong country is as expected");
  }

  /** @dev This test checks if setting favourites by the user is successful
  */
  function testSettingFavorite() public {
    string memory oldFavorite = distributer.favouritesIPFSHash(this);
    string memory expected;
    Assert.equal(oldFavorite, expected, "Favorite list of user is as expected");
    
    expected = "QmR7GSQM93Cx5eAg6a6yRzNde1FQv7uL6X1o4k7zrJa3LX";
    bool result = distributer.alterFavourite(expected);
    Assert.isTrue(result, "Setting favourite should be successful");
    string memory newFavorite = distributer.favouritesIPFSHash(this);
    Assert.equal(newFavorite, expected, "Favorite list of user is as expected");
  }

}



