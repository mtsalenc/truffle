import { Routes,
     RouterModule } from '@angular/router';
import { UserComponent } from './user.component';
import {NgModule} from '@angular/core';



const routes: Routes = [
  {
    path: 'user',
    component: UserComponent,
    data: {
      title: 'User'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule {}
