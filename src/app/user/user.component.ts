import { Component, OnInit, TemplateRef } from '@angular/core';
import { Web3Service } from '../util/web3.service';
import { countries } from './countries';

import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { AlertComponent } from 'ngx-bootstrap/alert/alert.component';

import {
  FormGroup,
  FormControl,
  Validators,
} from '@angular/forms';

declare let require: any;
const distributor_artifacts = require('../../../build/contracts/Distributer.json');
const newspaper_artifacts = require('../../../build/contracts/NewsPaper.json');

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})

export class UserComponent implements OnInit {
  modalRef: BsModalRef;
  config = {
    animated: true
  };
  countries = {};
  countryNames = [];
  accounts: string[];
  categories = ['General', 'Education', 'Entertainment'];
  papers: Array<Object>;
  paperConfig = {
    count: 0,
    pageSize: 10,
    current: 0
  }
  favs = [];

  model = {
    amount: 5,
    receiver: '',
    balance: 0,
    account: '',
    balanceText: '',
    ownedPapers: [],
    chosenPaper: ''
  };

  myform: FormGroup;

  paper = {
    address: '',
    name: '',
    category: '',
    state: '',
    country: ''
  }

  chosenFields = {
    country: '0',
    state: '0',
    category: '0'
  }

  status = '';

  Distributor: any;
  NewsPaper: any;

  constructor(private web3Service: Web3Service, private modalService: BsModalService) {
    this.countries = countries;
    this.countryNames = Object.keys(this.countries);
    this.myform = new FormGroup({
      name: new FormControl('', [Validators.required, Validators.max(50)]),
      country: new FormControl('', [
        Validators.required, Validators.max(25)
      ]),
      state: new FormControl('', [
        Validators.required, Validators.max(25)
      ]),
      category: new FormControl('', [
        Validators.required, Validators.max(20)
      ]),
    });

  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, this.config);
  }


  async ngOnInit() {
    this.watchAccount();
    this.web3Service.artifactsToContract(distributor_artifacts)
      .then((DistributorAbstraction) => {
        this.Distributor = DistributorAbstraction;
      });
    this.web3Service.artifactsToContract(newspaper_artifacts)
      .then((NewsPaperAbstraction) => {
        this.NewsPaper = NewsPaperAbstraction;
      });
      this.accounts = await this.web3Service.getAccounts();
      this.model.account = this.accounts[0];
      this.refreshBalance(this.model.account);
  }

  watchAccount() {
    this.web3Service.accountsObservable.subscribe(async (accounts) => {
      let network = await this.web3Service.getNetwork();
      if(network!==4){
        alert('This service works only in Rinkenby network if your using the rinkeby setup. Please install metamask google chrome extension or switch to rinkenby network in metamask');
      }
      this.accounts = accounts;
      this.refreshBalance(accounts[0]);
    });
  }

  async refreshBalance(account) {
    this.model.account = account;
    console.log('Refreshing balance');
    this.model.balanceText = 'Fetching value';
    const balance = await this.web3Service.getAccountEtherBalance(this.model.account);
    if (balance === null) {
      this.model.balance = 0;
      this.model.balanceText = "Couldn't fetch";
    } else {
      this.model.balance = balance;
      this.model.balanceText = balance + ' Ether';
      this.getOwnedNewsPapers();
      this.getDefaultNewsPapers();
      this.getFavourites();
    }
  }

  setStatus(type, status) {
    this.alerts.push({
      type: type,
      msg: status,
      timeout: 2000
    });
  }
  async getDefaultNewsPapers(){
    if (!this.Distributor) {
      this.setStatus('danger', 'Contract is not loaded, unable to send transaction');
      return;
    }
    try {
      let papers = [];
      const deployed = await this.Distributor.deployed();
      let result = await deployed.getPapersCount.call({ from: this.model.account });
      this.paperConfig.count = parseInt(result);
      let limit = (this.paperConfig.current + this.paperConfig.pageSize);
      if(limit>this.paperConfig.count){
        limit = this.paperConfig.count;
      }
      let i = 0;
      for (i = this.paperConfig.current; i < limit; i++) {
        let paper = await deployed.newsPapers.call( i, { from: this.model.account });
        let paperInfo = await this.getPaperinformation(paper);
        paperInfo['address'] = paper;
        papers.push(paperInfo);
      }
      this.paperConfig.current = i;
      if(this.papers!==undefined){
        this.papers = this.papers.concat(papers)
      } else {
        this.papers= papers;
      }
    } catch (e) {
      console.log(e);
      this.setStatus('danger','Error sending coin; see log.');
    }
  }

  async getOwnedNewsPapers() {
    if (!this.Distributor) {
      this.setStatus('danger', 'Contract is not loaded, unable to send transaction');
      return;
    }
    try {
      const deployed = await this.Distributor.deployed();
      let result = await deployed.getOwnedCount.call({ from: this.model.account });
      result = parseInt(result);
      this.model.ownedPapers = [];
      for (let i = 0; i < result; i++) {
        let paper = await deployed.ownedPapers.call(this.model.account, i, { from: this.model.account });
        this.model.ownedPapers.push(paper);
      }
      if (result > 0) {
        this.getNewsPaperDetails(this.model.ownedPapers[0]);
      } else {
        this.paper.name = "";
        this.paper.category = "";
        this.paper.state = "";
        this.paper.country = "";
      }

    } catch (e) {
      console.log(e);
      this.setStatus('danger','Error sending coin; see log.');
    }
  }

  async getFavourites() {
    if (!this.Distributor) {
      this.setStatus('danger', 'Contract is not loaded, unable to send transaction');
      return;
    }
    try {
      const deployed = await this.Distributor.deployed();
      let result = await deployed.favouritesIPFSHash.call(this.model.account, { from: this.model.account });
      if(result!==""&&result!==undefined&&result!==null){
        let data = await this.web3Service.getContentFromIPFS(result);
        localStorage.setItem("favorites", data["_body"])
        data =  JSON.parse(data["_body"]);
        for(let key in data){
          this.favs.push(data[key])
        }
        console.log(this.favs)
      }

    } catch (e) {
      console.log(e);
      this.setStatus('danger','Error sending coin; see log.');
    }
  }

  async getPaperinformation(paperAddress) {
    const deployed = await this.NewsPaper.at(paperAddress);
    const info = await deployed.information.call({ from: this.model.account });
    return {
      name: info[0],
      country: info[1],
      state: info[2],
      category: info[3]
    }
  }

  async getNewsPaperDetails(chosenPaper) {
    this.model.chosenPaper = chosenPaper;
    if (!this.NewsPaper) {
      this.setStatus('danger','Contract is not loaded, unable to send transaction');
      return;
    }
    this.setStatus('info','Getting Chosen Paper Details... (please wait)');
    try {
      let paper = await this.getPaperinformation(this.model.chosenPaper);
      this.paper.address = chosenPaper;
      this.paper.name = paper.name;
      this.paper.state = paper.state;
      this.paper.country = paper.country;
      this.paper.category = paper.category;

    } catch (e) {
      console.log(e);
      this.setStatus('danger','Error sending coin; see log.');
    }
  }

  setAmount(e) {
    console.log('Setting amount: ' + e.target.value);
    this.model.amount = e.target.value;
  }

  setReceiver(e) {
    console.log('Setting receiver: ' + e.target.value);
    this.model.receiver = e.target.value;
  }

 

  getSearchString() {
    let searchString = '';
    let type = 0;
    if (this.chosenFields.country !== '0') {
      type = 1;
      searchString = this.chosenFields.country;
    }
    if (this.chosenFields.state !== '0') {
      type = 2;
      searchString += this.chosenFields.state;
    }
    if (this.chosenFields.category !== '0') {
      if (type === 2) {
        type = 4;
      } else {
        type = 3;
      }
      searchString += this.chosenFields.category;
    }
    let hash = this.web3Service.getHash(searchString);
    return {
      hash: hash,
      type: type
    };
  }

  async getPapers(contract, functionName, hash, count) {
    let papers = [];
    if (count > 0) {
      for (let i = 0; i < count; i++) {
        let paper = await contract[functionName].call(hash, i, { from: this.model.account });
        let paperInfo = await this.getPaperinformation(paper);
        paperInfo['address'] = paper;
        papers.push(paperInfo);
      }
    }
    return papers;
  }

  async search() {
    if (!this.Distributor) {
      this.setStatus('danger','Contract is not loaded, unable to send transaction');
      return;
    }
    let searchString = this.getSearchString();
    const deployed = await this.Distributor.deployed();
    let result;
    let functionToCall = '';
    if (searchString.type === 4) {
      result = await deployed.getLocationCategoryPaperCount.call(searchString.hash, { from: this.model.account });
      functionToCall = 'locationCategoryIndex';
    } else if (searchString.type === 3) {
      functionToCall = 'categoryIndex';
      result = await deployed.getCategoryPaperCount.call(searchString.hash, { from: this.model.account });
    } else if (searchString.type === 2) {
      result = await deployed.getLocationPaperCount.call(searchString.hash, { from: this.model.account });
      functionToCall = 'locationIndex';
    } else {
      result = await deployed.getCountryWisePaperCount.call(searchString.hash, { from: this.model.account });
      functionToCall = 'countryIndex';
    }
    result = parseInt(result);

    this.papers = await this.getPapers(deployed, functionToCall, searchString.hash, result)

  }

  async submit(value){
    console.log(value)

    if (!this.Distributor) {
      this.setStatus('danger','Contract is not loaded, unable to send transaction');
      return;
    }
    let searchString = this.getSearchString();
    const deployed = await this.Distributor.deployed();
    alert("Submitting transaction");
    let result = await deployed.createANewsPaper(value.name, value.country, value.state, value.category, {from: this.model.account, gas: 4071915});
    if(result.logs.length===2){
      this.getOwnedNewsPapers();
      alert("Newspaper Created successfully");
    }
  }

  alerts: any[] = [];
 
 
  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }
}
