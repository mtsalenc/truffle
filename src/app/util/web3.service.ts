import { Injectable } from '@angular/core';
import * as contract from 'truffle-contract';
import { Subject } from 'rxjs';
declare let require: any;
import { Http } from '@angular/http';

const Web3 = require('web3');


declare let window: any;

@Injectable()
export class Web3Service {
  warn = false;
  private web3: any;
  private accounts: string[];
  public ready = false;
  public accountsObservable = new Subject<string[]>();

  constructor(private http: Http) {
    window.addEventListener('load', (event) => {
      this.bootstrapWeb3();
    });
  }

  public bootstrapWeb3() {
    // Checking if Web3 has been injected by the browser (Mist/MetaMask)
    if (typeof window.web3 !== 'undefined') {
      // Use Mist/MetaMask's provider
      this.web3 = new Web3(window.web3.currentProvider);
    } else {
      console.log('No web3? You should consider trying MetaMask!');

      // Hack to provide backwards compatibility for Truffle, which uses web3js 0.20.x
      Web3.providers.HttpProvider.prototype.sendAsync = Web3.providers.HttpProvider.prototype.send;
      // fallback - use your fallback strategy (local node / hosted node + in-dapp id mgmt / fail)
      this.web3 = new Web3(new Web3.providers.HttpProvider('http://localhost:8545'));
    }

    setInterval(() => this.refreshAccounts(), 100);
  }

  public async artifactsToContract(artifacts) {
    if (!this.web3) {
      const delay = new Promise(resolve => setTimeout(resolve, 100));
      await delay;
      return await this.artifactsToContract(artifacts);
    }

    const contractAbstraction = contract(artifacts);
    contractAbstraction.setProvider(this.web3.currentProvider);
    return contractAbstraction;

  }

  private refreshAccounts() {
    this.web3.eth.getAccounts((err, accs) => {
      console.log('Refreshing accounts');
      if (err != null) {
        console.warn('There was an error fetching your accounts.');
        return;
      }

      // Get the initial account balance so it can be displayed.
      if (accs.length === 0) {
        if (this.warn === false) {
          alert('Error connecting to your ethereum client. Please make sure your ethereum client is running or Metamask is logged in.');
          this.warn = true;
        }
        console.warn('Couldn\'t get any accounts! Make sure your Ethereum client is configured correctly.');
        return;
      }

      if (!this.accounts || this.accounts.length !== accs.length || this.accounts[0] !== accs[0]) {
        console.log('Observed new accounts');

        this.accountsObservable.next(accs);
        this.accounts = accs;
      }

      this.ready = true;
    });
  }

  public async getAccountEtherBalance(address) {
    if (this.web3) {
      let balance = await this.web3.eth.getBalance(address);
      balance = this.web3.utils.fromWei(balance, 'ether')
      return balance;
    } else {
      return null;
    }
  }

  getHash(string) {
    return this.web3.utils.keccak256(string);
  }

  convertAmount(amount, factor) {
    return this.web3.utils.toWei(amount, factor);
  }

  convertAmountFromWei(amount, factor) {
    return this.web3.utils.fromWei(amount, factor);
  }

  convertBigNumberDateToLocale(data) {
    const date = new Date(data * 1000);
    return date.toLocaleString();
  }

  getAccounts() {
    return this.web3.eth.getAccounts();
  }

  getNetwork() {
    return this.web3.eth.net.getId();
  }

  getContentFromIPFS(hash) {

    return this.http.get('https://ipfs.infura.io/ipfs/' + hash).toPromise();

  }

  postToIPFS(data) {

    return this.http.post('https://ipfs.infura.io:5001/api/v0/add', data).toPromise();

  }

}
